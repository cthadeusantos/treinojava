// 2019.1.1

import java.util.*;

class Entrada {
    String nOrigem, nTraducao;
    String iOrigem, iTraducao;

    Entrada (String palavra1, String palavra2, String idioma1, String idioma2){
        this.nOrigem = palavra1;
        this.nTraducao = palavra2;
        this.iOrigem = idioma1;
        this.iTraducao = idioma2;
    }
}

class Tradutor {
    List<Entrada> lista;
    Tradutor(){
      lista  = new ArrayList<Entrada>();
    }
    public void adiciona(Entrada chave){
        lista.add(chave);
    }
    public String geraEntradas(String origem, String destino){
        String saida = origem + "->" + destino + "\n";
        for (Entrada item : lista){
            //saida = saida + item.nOrigem;
            if((origem == item.iOrigem) && (destino == item.iTraducao)){
                saida += (item.nOrigem + "->"+item.nTraducao+"\n");
            }
            if((origem == item.iTraducao) && (destino == item.iOrigem)){
                saida += (item.nTraducao + "->"+item.nOrigem+"\n");
            }
        }
        return saida;
    }
}
public class Programa{
    public static void main(String[] args){
        Tradutor dic = new Tradutor();
        dic.adiciona(new Entrada("carro", "car", "pt", "en"));
        dic.adiciona(new Entrada("table", "mesa", "en", "pt"));
        dic.adiciona(new Entrada("voiture", "carro", "fr", "pt"));
        dic.adiciona(new Entrada("dog", "cachorro", "en", "pt"));
        dic.adiciona(new Entrada("pao", "pane", "it", "pt"));
        System.out.println(dic.geraEntradas("pt", "en"));
    }
}