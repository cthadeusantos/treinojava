import java.util.Scanner;

public class AP2016_2_Q1 {
    static private int m;
    static private int n;
    static private int acumulador;
    public static void main(String[] args){
        //int m;
        //int n;
        int i, j;
        // converte dados de parametros
        m = Integer.parseInt(args[0]);
        n = Integer.parseInt(args[1]);
        // constroi a matriz
        String s[][] = constroiMatriz(m,n);
        for (i = 0; i < m; i++){
            for (j = 0; j < n; j++){
                System.out.print(s[i][j]);
                System.out.print(" ");
            }
            System.out.println("");
        }
        numerico(s);

    }
    private static String[][] constroiMatriz(int m, int n){
        int i,j;
        String [][] matriz = new String[m][n];
        for (i = 0; i < m; i++){
            for (j = 0; j < n; j++){
                if ( i == j )
                    matriz[i][j] = "=";
                if ( i < j )
                    matriz[i][j] = "<";
                if ( i > j )
                    matriz[i][j] = Integer.toString( i + j );
            }
        }
        return matriz;
    }
    private static void numerico(String[][] matriz){
        int i, j;
        acumulador = 0;
        for (i = 0; i < m; i++){
            for (j = 0; j < n; j++){
                if (matriz[i][j].matches("[0-9]*")) acumulador++; 
            }
        }
        System.out.println(acumulador);
    }
}

