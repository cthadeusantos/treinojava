class Pilha{
    private static int TAM_MAX = 1000;
    private int valores[];
    private int topo;

    public Pilha(){
        super();
        topo = -1;
        valores = new int[TAM_MAX];
    }
    public boolean empty(){
        if (topo != -1){
            return true;
        } else {
            System.out.println("Pilha Vazia");
            return false;
        }
    }
    public void push(int i){
        if ((topo+1) < TAM_MAX){
            valores[++topo] = i;
        } else {
            System.out.println("Pilha cheia");
        }
    }
    public void pop(){
        if (this.empty()){
            System.out.printf("Removendo %d",valores[topo--]);
        }
    }
    public void papa_e_pop(int i){
        int j;
        if ((i < topo+1) && i > 0){
            for (j = 0; j < i; j++){
                System.out.printf("Removendo %d\n", valores[topo--]);
            }
        } else {
            System.out.println("Valor inválido");
        }
    }
    public void top(){
        System.out.printf("Topo %d\n", valores[topo]);
    }
}

public class Principal{
    public static void main(String[] args){
        Pilha pilha = new Pilha();
        pilha.empty();
        pilha.push(10);
        pilha.push(20);
        pilha.push(30);
        pilha.papa_e_pop(2);
        pilha.top();
    }
}
