// Dado um numero n
// calcule todos os numero primos até ele

class Primo {
    
    public static void main(String[] args){
        int numero = Integer.parseInt(args[0]);
        if (numero < 2){
            //System.exit(-1);
            throw new IllegalArgumentException("Primos precisam ser maiores ou iguais a 2");
        }
        int contador;
        int primos[] = new int[numero];
        contador = 0;
        primos[contador++]=2;
        for(int i = 3; i <= numero; i = i + 2){
            boolean valid = true;
            for (int j = 3; j <= i; j = j + 2){
                if ((i % j) == 0 && i != j){
                    valid = false;
                    j = i+1;
                }
            }
            if (valid){
                primos[contador++] = i;
            }
        }
        for (int i=0; i < contador; i++){
            System.out.print(primos[i]+" ");
        }

    }
}