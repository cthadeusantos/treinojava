public class Despesa{
    private String CPF;
    private float[] vetor;
    public Cliente(String CPF, float[] vetor){
        this.CPF = CPF;
        this.vetor[] = vetor;
    }
    public String getCPF(){
        return this.CPF;
    }
    public float totalizaMes(int mes){
        int comprimento;
        int acumulado;
        comprimento = this.vetor.length;
        acumulado = 0;
        for (i = 0; i < comprimento; i++){
            if (mes == this.vetor[i].mes) acumulado += this.vetor[i].valor;
        }
        return acumulado;
    }
}