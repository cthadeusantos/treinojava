// Questao 1
// AP1 2017.1

class Data{
    private int dia;
    private int mes;
    private int ano;
    public Data(){
        this.dia = 1;
        this.mes = 1;
        this.ano = 1900;
    }
    public Data(int dia, int mes, int ano){
        if (mes < 1) mes = 1;
        if (mes > 12) mes = 12;

        if (mes < 1) mes = 1;
        if (mes > 12) mes = 12;
        if ((mes == 2) && (dia > 28)) dia = 28;
        if (
            ((mes == 1) || (mes == 3) || (mes == 5) ||
            (mes == 7) || (mes == 8) || (mes == 10) || 
            (mes == 12)) && (dia > 31)
            ) dia = 31;
        if (
            ((mes == 4) || (mes == 6) || (mes == 9) ||
            (mes == 11)) && (dia > 30)
            ) dia = 30;
        this.ano = ano;            
        this.mes = mes;
        this.dia = dia;
    }
    public void incrementar(){
        this.dia++;
        if ((this.mes == 2) && (this.dia > 28)) {
            this.mes++;
            this.dia = 1;
        }
        if (((mes == 1) || (mes == 3) || (mes == 5) ||
            (mes == 7) || (mes == 8) || (mes == 10) || 
            (mes == 12)) && (dia > 31)) {
                this.mes++;
                if (this.mes > 12) this.mes = 1;
                this.dia = 1;
            }
        if (((mes == 4) || (mes == 6) || (mes == 9) ||
            (mes == 11)) && (dia > 30)) {
                this.mes++;
                this.dia = 1;
            }
    }
    public String toString(){
        String[] extenso = {"Janeiro", "Fevereiro", "Março", "Abril",
                            "Maio","Junho","Julho","Agosto","Setembro",
                            "Outubro","Novembro","Dezembro"};
        return Integer.toString(dia) + " de " + extenso[mes-1] + " de " + Integer.toString(ano);
    }
} // Fim classe Data

public class Q1{
    public static void main(String[] args){
        Data a = new Data();
        Data b = new Data(2,3,2010);
        Data c = new Data(31,3,2010);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        c.incrementar();
        System.out.println(c);

    }
}