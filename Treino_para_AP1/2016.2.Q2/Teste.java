class Produto {
    private int codigo;
    private String nome;
    private double preco;
    public Produto(int codigo, String nome, double preco) {
        this.codigo = codigo;
        this.nome = nome;
        this.preco = preco;
    }
    public int getCodigo() {
    return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public double getPreco() {
        return preco;
    }
    public void setPreco(double preco) {
        this.preco = preco;
    }
}

// Classe CarrinhoDeCompra redefinada para AP1_2016_2_Q2
class CarrinhoDeCompra {
    private final int MAX_SIZE = 99;
    int itens_carrinho;
    private Produto[] produtos;
    // Construtor
    public CarrinhoDeCompra(){
        super();
        produtos = new Produto[MAX_SIZE];
        itens_carrinho = 0;
    }
    public void add(Produto p){
        if (this.itens_carrinho >= MAX_SIZE) {
            System.out.println("Carrinho lotado, não posso adicionar!");
            return;
        }
        produtos[itens_carrinho] = p;
        this.itens_carrinho++;
    }
} // Fim Classe CarrinhoDeCompra

class Teste {
    public static void main(String[] args) {
        int x;
        Produto produto1 = new Produto(1,"Produto 1", 10.2F);
        Produto produto2 = new Produto(2,"Produto 2", 20.2F);
        Produto produto3 = new Produto(3,"Produto 3", 30.2F);
        System.out.println(produto1.getNome());
        CarrinhoDeCompra carrinho1 = new CarrinhoDeCompra();
        for (x=0;x<35;x++){
            carrinho1.add(produto1);
            carrinho1.add(produto2);
            carrinho1.add(produto3);
        }
    }
}