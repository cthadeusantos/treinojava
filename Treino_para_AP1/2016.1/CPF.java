public class CPF {
    private String cpf;
    public CPF(String CPF){
        if (this.check(CPF)) {
            this.cpf=CPF;
        } else {
            this.cpf=null;
        } 
    }
    public boolean check(String CPF){
        if (!(CPF.matches("[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}") || CPF.matches("[0-9]{11}"))) return false;   
        int i;
        int acumula;
        
        // Testa dígito verificador 1
        acumula = 0;
        String auxiliar = CPF.replace(".","").replace("-","");
        for (i = 0; i < 9; i++){
            var arr = auxiliar.charAt(i);
            int x = (int)arr - 48;
            acumula += ((i+1) * x);
        }
        int r = (acumula % 11) % 10;
        var arr = auxiliar.charAt(9);
        int x = (int)arr - 48;
        if (r != x) return false; 

    // Testa dígito verificador 2
        acumula = 0;   
        for (i = 1; i < 9; i++){
            arr = auxiliar.charAt(i);
            x = (int)arr - 48;
            acumula += ((i+1) * x);
        }
        r = (acumula % 11) % 10;
        arr = auxiliar.charAt(10);
        x = (int)arr - 48;
        if (r != x) return false;     
        return true;
    }
    public String getCPF(){
        return this.cpf;
    }
    public String toString(){
        if (this.cpf != null){
            return "CPF válido:" + this.cpf;
        } else {
            return "CPF Inválido!";
        }
        
    }
}