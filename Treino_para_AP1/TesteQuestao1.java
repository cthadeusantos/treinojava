abstract class Mensagem {
    protected String texto;
    public Mensagem(String texto) { this.texto = texto; }
    public String obterTexto() { return this.texto; }
    public void imprimir() { System.out.println(this.texto); }
    }
    class Email extends Mensagem {
    public Email(String texto) { super(texto); }
    }

    class FacebookChat extends Mensagem {
        public FacebookChat(String texto) { super(texto); }
        }
        public class TesteQuestao1 {
        public static void imprimirMensagens(final Mensagem[] msgs) {
        for (int i = 0; i < msgs.length; i++)
        msgs[i].imprimir();
        }
        public static int contarReferenciasDuplicadas (final Mensagem[]
        msgs) {
        int total = 0;
        boolean[] ignorar = new boolean[msgs.length];
        for (int i = 0; i < msgs.length; i++)
        ignorar[i] = (msgs[i] == null);
        for (int i = 0; i < msgs.length - 1; i++) {
        if (!ignorar[i]) {
        boolean duplicado = false;
        for (int j = i + 1; j < msgs.length; j++) {
        if ((!ignorar[j]) && (msgs[i] == msgs[j])) {
        ignorar[j] = true;
        duplicado = true;
        }
        }
        if (duplicado) total++;
        }
        }
        return total;
        }
        public static int contarMensagensDuplicadas (final Mensagem[]
        msgs) {
        int total = 0;
        boolean[] ignorar = new boolean[msgs.length];
        for (int i = 0; i < msgs.length; i++)
        ignorar[i] = (msgs[i] == null) || (msgs[i].obterTexto() == null);
        for (int i = 0; i < msgs.length - 1; i++) {
        if (!ignorar[i]) {
        boolean duplicado = false;
        for (int j = i + 1; j < msgs.length; j++) {
            if ((!ignorar[j]) &&
(msgs[i].obterTexto().equals(msgs[j].obterTexto()))){
ignorar[j] = true;
duplicado = true;
}
}
        if (duplicado) total++;
        }
    }
        return total;
    }
    public static void main(String args[]) {
        String textoEmail = "Olá,\n este é um e-mail de teste.";
        Email email = new Email(textoEmail);
        String textoChat = "Oi, vamos tc?";
        FacebookChat chat01 = new FacebookChat(textoChat);
        FacebookChat chat02 = new FacebookChat(textoChat);
        Mensagem[] mensagens = new Mensagem[5];
        mensagens[0] = email;
        mensagens[1] = chat01;
        mensagens[2] = email;
        mensagens[3] = chat02;
        mensagens[4] = chat02;
        imprimirMensagens(mensagens);
        System.out.println(contarReferenciasDuplicadas(mensagens));
        System.out.println(contarMensagensDuplicadas(mensagens));
    }
}