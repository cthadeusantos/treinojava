public class Lampada {
    // Atributo da lampada
    private boolean status;
    // contrutor
    // Lampada ligada , status=true
    // lampada desligada, status=false
    public Lampada (boolean status){
        this.status = status;
    }
    public void ligar(){
        if (!(this.status)) this.status = (!(this.status));
    }
    public void desligar(){
        if (this.status) this.status = (!(this.status));
    }
    public void imprimir(){
        if (this.status){
            System.out.println("Lâmpada ligada!");
        } else {
            System.out.println("Lâmpada desligada!");
        }
    }
}