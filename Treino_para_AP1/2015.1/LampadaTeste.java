public class LampadaTeste{
    public static void main(String[] args){
        Lampada lampada1 = new Lampada(true);
        lampada1.imprimir();
        lampada1.desligar();
        lampada1.imprimir();
        lampada1.ligar();
        lampada1.imprimir();
        lampada1.ligar();
        lampada1.imprimir();
        lampada1.desligar();
        lampada1.imprimir();
    }
}

