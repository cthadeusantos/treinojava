class Ponto{
    protected double x,y;
    public Ponto(double x, double y){
        this.x = x;
        this.y = y;
    }
}

class Ponto3D extends Ponto{
    protected double z;
    public Ponto3D(double x, double y, double z){
        super(x, y);
        this.z = z;
    }
    public Ponto3D diferenca(Ponto3D ponto){
        double difx = ponto.x - this.x;
        double dify = ponto.y - this.y;
        double difz = ponto.z - this.z;
        return new Ponto3D(difx, dify, difz);
    }
    public double distancia(Ponto3D ponto){
        return Math.sqrt(
                        Math.pow(ponto.x - this.x, 2)+
                        Math.pow(ponto.y - this.y, 2)+
                        Math.pow(ponto.z - this.z, 2)
                        );
    }
    public String toString(){
        return "("+this.x+","+this.y+","+this.z+")";
    }
}

public class Q3{
    public static void main(String[] args){
//         Q3.java:32: error: cannot find symbol
//         Ponto3D a = Ponto3D(1.0,3.0,3.0);
//                     ^
//   symbol:   method Ponto(double,double,double)
//   location: class Q3
// Q3.java:33: error: cannot find symbol
//         Ponto3D b = Ponto3D(1.0,3.0,3.0);
//                     ^
//   symbol:   method Ponto(double,double,double)
//   location: class Q3
//
// O esquecimento de new causou o erro acima

        Ponto3D a = new Ponto3D(1.0,3.0,5.0);
        Ponto3D b = new Ponto3D(1.0,7.0,3.0);
        System.out.println(a.distancia(b));
        Ponto3D c = a.diferenca(b);
        System.out.println(c);       
    }
}