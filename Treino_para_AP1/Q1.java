// AP1 2011.2 Questao 1
public class Q1 {
    public static void main(String[] args){
        String palavra = args[0];
        int tamanho = palavra.length();
        int rotacao = 0;
        for (int i = 0; i < tamanho; i++){
            for (int j = 0; j < tamanho; j++){
                if (( i + j) < tamanho){
                    rotacao = i + j;
                } else {
                    rotacao = (i + j) - (tamanho);
                }
                System.out.printf("%c", palavra.charAt(rotacao));
            }
            System.out.printf("\n");
        }
    }
}