public class Retangulo{
    float base1;
    float altura;
    public Retangulo(float base1, float altura){
        this.base1 = base1;
        this.altura = altura;
    }
    public float area(){
        return base1 * altura;
    }
    public float perimetro(){
        return (2 * base1) + (2 * altura);
    }
}
public class Caixa extends Retangulo{
    float base2;
    public Caixa(float base1, float base2, float altura){
        super(base1,altura);
        this.base2 = base2;
    }
    public float volume(){
        return base1 * base2 * altura;
    }
    public float area(){
        return (2 * (base1 * base2 + base1 * altura));
    }
}

class Circulo {
    float raio;
    public Circulo(float raio){
        this.raio = raio;
    }
    public float area(){
        return 3.14 * pow(raio,2);
    }
    public float perimetro(){
        return 2 * 3.14 * raio;
    }
}

class 