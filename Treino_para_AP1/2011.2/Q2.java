abstract class Publicacao{
    String titulo;
    float preco_compra;
    public Publicacao(String titulo, float preco_compra){
        this.titulo = titulo;
        this.preco_compra = preco_compra;
    }
    public float retornaPreco(){
        return this.preco_compra;
    }
}

class Livro extends Publicacao{
    int isbn;
    String autor;
    String editora;
    java.util.GregorianCalendar datapublicacao;
    public Livro(int isbn, String titulo, String autor, String editora,
    java.util.GregorianCalendar datapublicacao, float preco_compra){
        super(titulo, preco_compra);
        this.isbn = isbn;
        this.autor = autor;
        this.editora = editora;
        this.datapublicacao = datapublicacao;

    } 
}

class Revista extends Publicacao{
    int mes;
    int ano;
    public Revista(String titulo, float preco_compra, int mes, int ano){
        super(titulo, preco_compra);
        this.mes = mes;
        this.ano = ano;
    }
}

class Gibi extends Publicacao {
    int isbn;
    int edicao;
    public Gibi(String titulo, float preco_compra, int isbn, int edicao){
        super(titulo, preco_compra );
        this.isbn = isbn;
        this.edicao = edicao;
    }
}

class Digital extends Publicacao {
    int issn;
    int tamanho;
    public Digital(String titulo, float preco_compra, int issn, int tamanho){
        super(titulo, preco_compra);
        this.issn = issn;
        this.tamanho = tamanho;
    }
}

public class Q2 {
    public static void main(String[] args){
        Publicacao x = new Revista("Falcão", 13.0f, 12,12);
        System.out.println(x.retornaPreco()); 
    }
}