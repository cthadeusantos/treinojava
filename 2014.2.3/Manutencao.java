class Manutencao implements Gastos{
    String peca;
    double custo;
    public Manutencao(String peca, double custo) {
         this.peca = peca;
         this.custo = custo;
    }
    public double getCusto(){
        return this.custo;
    }
}