class Abastecimento implements Gastos{
    float valor;
    String posto;
    public Abastecimento(float valor, String posto) { 
        this.posto = posto;
        this.valor = valor;
    }
    public double getCusto(){
        return this.valor;
    }
}