import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.regex.*;

// AP2 2015.1.2

class Numero{
    private int numero;
    Numero(String valor){
        String regex1 = "[0-9]+";
        if (!(valor.matches(regex1))){
            throw new IllegalArgumentException("Bad value!");
        }
        this.numero = Integer.parseInt(valor);;       
    }
    public String converte(){
        String binario = "";
        int inteiro;
        int resto;
        inteiro = this.numero;
        while (inteiro != 0){
            inteiro = this.numero / 2;
            resto = this.numero % 2;
            binario = Integer.toString(resto) + binario;
            this.numero = inteiro;
        }
        return binario;
    }
    public String toString(){
        return Integer.toString(this.numero) + " em binário vale: " + this.converte();
    }
}

public class Programa{
    public static void main(String[] args){
        try{
            FileReader in = new FileReader(args[0]);
            BufferedReader buffer = new BufferedReader(in);
            String linha;
            String regex2 = "[Ff]?im";
            while (!(linha = buffer.readLine()).equals("Fim")){
                    Numero num = new Numero(linha);
                    //String convertido = num.converte();
                    System.out.println(num);
            }
            in.close();
            buffer.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
