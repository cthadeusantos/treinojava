// Classe concreta Pasta
// Importa módulos necessários
import java.util.ArrayList;

public class Pasta extends SistemaArquivo{
    // arrayList que conterá todos os elementos adicionados
    // os elementos adicionados serão instâncias adicionadas com o método .add
    ArrayList<SistemaArquivo> listaDeElementos = new ArrayList<>();
    
    // Construtor - Cria a instância de pasta
    public Pasta(String nome) {
        super(nome); 
    }
    
    //
    // Métodos públicos
    //

    // Método adiciona
    public void adiciona(SistemaArquivo elemento){
        // Verifica o tipo da instância
        // Arquitetura de software não recomenda "padrão polimorfismo" para selecionar o comportamento
        // A responsabilidade deveria ser posta em diferentes classes
        //  mas eu não sei como implementar Hashtable ou Hashmap, logo vou usar :
        if (elemento instanceof Arquivo) {                  // Se for Arquivo
            listaDeElementos.add(elemento);                 // Adiciona arquivo a lista
        } else if (elemento instanceof Pasta) {             // Pasta adicionada a pasta ganha pasta pai
            listaDeElementos.add(elemento);
        } else {   
            // Caso de entrada inválida
            System.out.println("Entrada inválido!" + this.nome);
        }
    }
    // Método recupera tamanho da pasta
    public int getTamanho(){
        int acumula = 0;
        for (SistemaArquivo item : this.listaDeElementos){  // loop for aprimorado Java Como Programar 10a edicao página 206
            if (item instanceof Arquivo) {                  // Se for Arquivo
                acumula = acumula + item.tamanho;
            } else if (item instanceof Pasta) {             // Se for Pasta
                acumula = acumula + item.getTamanho();      // acumula atraves de recursao
            }
        }        
        return acumula;
    }

    // Método remove arquivo ou pasta no diretório raiz
    public void remove(String value){
        String[] parts = value.split("/");                          // divide a string
        int ultimo = parts.length - 1;
        int indice = 0;                                             // índice que representa o elemento consultado na listaDeElementos
        for (SistemaArquivo item : this.listaDeElementos){          // For aprimorado que percorre a lista de elementos da raiz
            if (item instanceof Pasta){                             // Se Pasta achada na raiz
                if (item.nome.equals(parts[ultimo])){               // Se item(Pasta) igual ao valor passado, entao apague-a!
                    this.listaDeElementos.set(indice, null);        // comando para apagar a pasta
                    return;
                }
                if (ultimo > 0){                                    // indice >0 significa Elemento buscado nao está na raiz
                    if (eraseInsideElement(((Pasta)item), parts)) {           // Se Pasta achada na raiz, acesse-a!
                        return;                                     // Elemento buscado foi removido
                    };                 
                }
            }
            if (item instanceof Arquivo && item.nome.equals(parts[ultimo])){  // Achei um Arquivo na raiz
                this.listaDeElementos.set(indice, null);
                return;
            }
            if (!(item instanceof Pasta) && !(item instanceof Arquivo)){    // adicionei exceção 
                System.out.println("Erro em tempo de execução! A instância precisa ser uma Pasta.");
                return;
            }
            indice++;
        }
        System.out.println("Nao foi possivel encontrar " + value);  // Percorreu toda a lista e nao encontrou elemento buscado
        return;
    }    

    // Método toString
    public String toString(){
        return showDirectory();
    }

    //
    // Métodos privados
    //
    
    // Método para montar a árvore a ser apresentada na tela
    // Lê elementos do diretório raiz fornecido
    private String showDirectory(){
        String string = this.nome;
        String tabulacao = "\t";
        for (SistemaArquivo item : this.listaDeElementos){
            if (item instanceof Pasta){
                // Se possuir subdiretórios
                string = string + "\n\t" + buildInsideDirectory((Pasta)item, tabulacao);
            }
            if (item instanceof Arquivo){ // Se existirem arquivos no diretório raiz fornecido
                string = string + tabulacao + item.nome + "\n";
            }
        }
        return string;
    }
    // Método que acessa os subdiretórios
    // Tabulação montada em tempo de execução
    private String buildInsideDirectory(Pasta Lista, String tabulacao){
        String string = Lista.nome;
        tabulacao = tabulacao + "\t";
        for (SistemaArquivo item : Lista.listaDeElementos){
            if (item instanceof Arquivo && item.nome!=""){
                // Se existir arquivo no subdiretório adiciona a variavel
                string =  string + "\n" + tabulacao  + item.nome;
            }
            if (item instanceof Pasta && item.nome!=""){
                // Se existir subdiretório entra e percorre o mesmo
                string = string + "\n" + tabulacao + buildInsideDirectory((Pasta)item, tabulacao) + "\n";
                return string;
            }
        }       
        return string;
    }

    // Método percorre arvore
    // Percorre a árvore de diretórios pesquisando o elemento a ser removido
    private boolean eraseInsideElement(Pasta Lista, String[] parts){
        int ultimo = parts.length - 1;                          // ultimo elemento do vetor
        int indice = 0;                                         // indice do elemento da lista sendo percorrida
        for (SistemaArquivo item : Lista.listaDeElementos){     // Percorre a lista em for aprimorado
            if (item instanceof Arquivo &&
                item.nome.equals(parts[ultimo] )){              // Se item for Arquivo e o item conferir com o elemento a ser removido
                //Lista.listaDeElementos.remove(parts[ultimo]); // não funciona
                Lista.listaDeElementos.set(indice, null);       // FUNCIONOU - REMOVE O ELEMENTO parts[ultimo] da lista - representado aqui pelo seu indice
                //item = null;                                  // não funciona
                return true;
            }
            if (item instanceof Pasta && item.nome.equals(parts[ultimo])){  // Se item for Pasta e o item com conferir com o elemento a ser removido
                //((Pasta)item).listaDeElementos.remove(parts[ultimo]);
                Lista.listaDeElementos.set(indice, null);
                return true;
            }
            if (item instanceof Pasta) {
                eraseInsideElement(((Pasta)item), this.deleteItemArray(parts, 0));
                return false;
            }
            indice++;
        }
        return false;
    }

    // Método remove item do array
    // Adaptado de alguns métodos achados na Internet para não perder tempo com isto aqui
    public String[] deleteItemArray(String[] arr, int ultimo){
        if (arr == null || ultimo < 0 || ultimo >= arr.length){
            return arr;
        }
        // Create another array of size one less 
        String[] anotherArray = new String[arr.length - 1];   
        // Copy the elements except the index 
        // from original array to the other array 
        for (int i = 0, k = 0; i < arr.length; i++) { 
            // if the index is 
            // the removal element index 
            if (i == ultimo) { 
                continue; 
            }
            // if the index is not 
            // the removal element index 
            anotherArray[k++] = arr[i]; 
        } 
        // return the resultant array 
        return anotherArray;         
    }
}