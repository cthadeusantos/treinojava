// Classe concreta que representa um arquivo
import java.util.regex.Pattern;                                     // importa biblioteca Regex
public class Arquivo extends SistemaArquivo{
    public Arquivo(String nome, int tamanho){
        super(nome, tamanho);
            if (!Pattern.matches(regexArquivoMode2, nome)){         // Levanta exceção se nome de arquivo for inválido
                throw new java.lang.RuntimeException("Nome de arquivo inválido!");
            }
    }
    // Retorna o tamanho do arquivo
    public int getTamanho(){
        return this.tamanho;
    }

}