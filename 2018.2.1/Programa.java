// 2018.2.1

import java.util.*;

interface Colecao {
    void adiciona (Object e);
    void remove (Object e);
    void lista();
}
class Vetor implements Colecao{
    final int MAX_SIZE = 1000;
    int contador;
    List<Object> vetor = null;
    Vetor(){
        vetor = new ArrayList<Object>();
        contador = 0;
    }

    public void adiciona(Object valor){
        if (contador < MAX_SIZE){
            contador++;
            vetor.add(valor);
        } else {
            System.out.println("Valor máximo do vetor atingido");
        }
    }
    
    public void remove(Object valor){
        if (contador >= 1){
            contador--;
            vetor.remove(valor);
        } else {
            System.out.println("Não há  elementos a remover");
        }
    }

    public void lista(){
        for (Object item:vetor){
            System.out.println(item);
        }
    }
}

public class Programa{
    public static void main(String[] args){
        Colecao colecao;
        colecao = new Vetor();
        String a = "A";
        String b = "B";
        String c = "C";
        colecao.adiciona(a);
        colecao.adiciona(b);
        colecao.adiciona(c);
        colecao.remove(b);
        colecao.lista();
    }
}
