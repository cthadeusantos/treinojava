// 2018.2.1

import java.util.*;

interface Colecao {
    void adiciona (Object e);
    void remove (Object e);
    void lista();
}
class Vetor implements Colecao{
    Object[] lista;
    int contador;
    
    Vetor(){
        lista = new Object[1000];
        contador = 0;
    }

    public void adiciona(Object valor){
        if (contador < 1000 && this.existe(valor) == -1){
            lista[contador] = valor;
            contador++;
        } else {
            System.out.println("Valor máximo do vetor atingido ou elemento já existe");
        }
    }
    
    private int existe(Object valor){
        int i;
        for (i = 0; i < contador; i++){
            if (lista[i].equals(valor)){
                return i;
            }
        }
        return -1;
    }

    public void remove(Object valor){
        if (contador == 0 && lista[contador]==null){
            System.out.println("Não há  elementos a remover");
            return;
        }
        int localiza = this.existe(valor);
        if (localiza > 0){
            contador--;
            lista[localiza] = lista[contador];
            lista[contador] = null;
        }
        if (localiza == 0){
            lista[localiza] = null;
        }
    }

    public void lista(){
        for (int i = 0; i < contador ; i++){
            System.out.println(lista[i]);
        }
    }
}

public class Programa2{
    public static void main(String[] args){
        Colecao colecao;
        colecao = new Vetor();
        String a = "A";
        String b = "B";
        String c = "C";
        colecao.adiciona(a);
        colecao.adiciona(b);
        colecao.adiciona(c);
        colecao.remove(b);
        colecao.lista();
    }
}
