// 2018.2.2

import java.io.*;
import java.util.*;
public class Gabarito{
  public static void main(String[] args) throws IOException{
    int n = Integer.parseInt(args[0]), i;
    if(n <= 0) return;
    String narq[] = new String[n];
for(i = 1; i <= n; i++) narq[i - 1] = args[i];
    Merge(n, narq, args[n + 1]);
  }
    static  void  Merge(int  n,  String[]  narq,  String  saida)  throws
IOException{
    BufferedReader in[] = new BufferedReader[n];
    for(int i = 0; i < n; i++)
      in[i] = new BufferedReader(new FileReader(narq[i]));
    BufferedWriter out;
    out = new BufferedWriter(new FileWriter(saida));
    String resp = "", s;
    int vet[] = new int[n];
    try {
      for(int i = 0; i < n; i++){
        s = in[i].readLine();
        if(s != null) vet[i] = Integer.parseInt(s);
        else vet[i] = Integer.MAX_VALUE;
      }
      int ind = menorInd(vet);
      while(ind != Integer.MAX_VALUE){
        resp = resp + vet[ind] + "\n";
        s = in[ind].readLine();
        if(s != null) vet[ind] = Integer.parseInt(s);
        else vet[ind] = Integer.MAX_VALUE;
        ind = menorInd(vet);
      }
      for(int i = 0; i < n; i++) in[i].close();
      out.write(resp);
      out.close();
    }
    catch (Exception e){
      System.out.println("Excecao\n");
    }
}
  static int menorInd(int[] vet){
    int resp = Integer.MAX_VALUE, menor = Integer.MAX_VALUE;
    for(int i = 0; i < vet.length; i++)
      if(menor > vet[i]){
        menor = vet[i];
resp = i; }
    return resp;
  }
}