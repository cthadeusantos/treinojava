import java.util.ArrayList;
import java.util.Collections;

class Campeonato{
    ArrayList<Clube> campeonato = new ArrayList<Clube>();
    //Construtor
    String nome1 = null;
    String[] score = null;
    String nome2 = null;
    public Campeonato(){
    }
    public void add(String nome1, String score, String nome2){
        this.nome1 = nome1;
        this.nome2 = nome2;
        this.score = score.split("x");
        if (existe(this.nome1) == -1){
            campeonato.add(new Clube(this.nome1));
        }
        if (existe(this.nome2) == -1){
            campeonato.add(new Clube(this.nome2));
        }
        this.vencedor();
    }

    public ArrayList<Clube> listagem(){
        // Ordena listagem
        for(int i = 0; i < (campeonato.size()); i++){
            for (int j = i; j < campeonato.size();j++){
                if (campeonato.get(i).pontos < campeonato.get(j).pontos){
                    Collections.swap(campeonato, i, j);
                }
            }
        }
        return campeonato;
    }
    public void vencedor(){
        String vencedor;
        if (Integer.parseInt(this.score[0]) > Integer.parseInt(this.score[1])){
            vencedor = this.nome1;
        } else {
            vencedor = this.nome2;
        }
        for (Clube item : campeonato){
            if (item.clube.equals(vencedor)){
                item.vitoria();
            }
        }
    }
    public int existe(String nome){
        int x = 0;
        for (Clube item : campeonato) {
            if (item.clube.equals(nome)){
                return x;
            }
            x = x + 1;
        }
        return -1;
    }
}