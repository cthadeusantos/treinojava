class Clube{
    String clube;
    int pontos;
    // Construtor
    Clube(String clube){
        this.clube = clube;
        this.pontos = 0;
    }
    public int saldo_pontos(){
        return this.pontos;
    }
    public void vitoria(){
        this.pontos = this.pontos + 1;
    }
}