import java.util.ArrayList;
import java.io.*;

class Programa{
    
    public static void main(String[] args){
        String partida;
        String[] vetor;
        Campeonato campeonato = new Campeonato();
        BufferedReader in = null;
        BufferedWriter out = null;
        try{
            in = new BufferedReader(new FileReader(args[0]));
            out = new BufferedWriter(new FileWriter("class"+args[0]));
            while ((partida = in.readLine()) != null){
                vetor = partida.split("#");
                campeonato.add(vetor[0],vetor[1],vetor[2]);
            }
            in.close();
            int x=0, posicao = 0;
            int pontuacao_anterior = -1;
            for (Clube item: campeonato.listagem()){
                x = x + 1;
                if (pontuacao_anterior!=item.pontos){
                    posicao=x;
                }
                pontuacao_anterior = item.pontos;
                String string = Integer.toString(posicao)
                + " "+item.clube
                + " "
                + Integer.toString(item.pontos)
                + "\n";
                out.write(string);
            }
            out.close();
        } catch (IOException e){
        }
    }
}