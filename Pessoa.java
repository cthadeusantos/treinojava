public class Pessoa {
	String nome;
	String perfil;

	Pessoa(String nome, String perfil){
		this.nome = nome;
		this.perfil = perfil;
	}
	
	private void combina(Pessoa p){
		if (this.perfil == p.perfil){
			System.out.println(this.nome + " aceita encontrar com " + p.nome);
		} else {
			System.out.println("Hum!" + p.nome + "  Sinto muito, você não faz o meu perfil");
		}
	}

	public static void main(String[] args){
		Pessoa a, b, c;
		a = new Pessoa("Astrogildo", "esportista");
		b = new Pessoa("Serafina", "esportista");
		c = new Pessoa("Agripina", "romantica");

		a.combina(b);
		b.combina(c);
		a.combina(c);
	}
}
