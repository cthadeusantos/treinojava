// AP 2 2019.1.2

import java.io.*;
import java.util.*;

interface FigGeom {
    public double area();
}

abstract class Figura implements FigGeom {
    public String tipo;
    protected double a, b;
    protected double area;

    Figura (double a){
        this.a = a;
    }

    Figura (double a, double b){
        this.a = a;
        this.b = b;
    }

    public abstract double area();
    public abstract String toString();
}

class Triangulo extends Figura{
    //private double area;

    Triangulo(double a, double b){
        super(a,b);
        this.area = area();
    }

    public double area(){
        return (super.a * super.b) / 2;
    }
    @Override
    public String toString(){
        return this.area + " " + this.tipo + " " + this.a + " " + this.b;
    }
}

class Quadrado extends Figura {
    Quadrado(double a){
        super(a);
        this.area = area();
    }

    public double area(){
        return Math.pow(super.a, 2);
    }
    @Override
    public String toString(){
        return this.area + " " + this.tipo + " " + this.a;
    }
}

class Retangulo extends Figura{
    Retangulo(double a, double b){
        super(a,b);
        this.area = area();
    }
    public  double area(){
        return super.a * super.b;
    }
    @Override
    public String toString(){
        return this.area + " " + this.tipo + " " + this.a + " " + this.b;
    }
}

public class Programa{
    public static void main(String[] args) throws IOException{
        List<Figura> lista;
        lista = new ArrayList<Figura>();
        Figura valor = null;
        FileReader arquivo = new FileReader(args[0]);
        BufferedReader in = new BufferedReader(arquivo);
        String linha;
        // Ler arquivo e processa
        while ((linha = in.readLine())!=null){
            String[] parametro = linha.split(" ");
            if (parametro[0].equals("R")){
                double a = Double.parseDouble(parametro[1]);
                double b = Double.parseDouble(parametro[2]);
                valor = new Retangulo(a, b);
                // adiciono o parametro tipo a instancia nao fiz setter
                // por causa de preguica
                valor.tipo = parametro[0]; 
            }
            if (parametro[0].equals("T")){
                double a = Double.parseDouble(parametro[1]);
                double b = Double.parseDouble(parametro[2]);
                valor = new Triangulo(a, b);
                // adiciono o parametro tipo a instancia nao fiz setter
                // por causa de preguica                
                valor.tipo = parametro[0]; 
            }
            if (parametro[0].equals("Q")){
                double a = Double.parseDouble(parametro[1]);
                valor = new Quadrado(a);
                // adiciono o parametro tipo a instancia nao fiz setter
                // por causa de preguica
                valor.tipo = parametro[0]; 
            }
            lista.add(valor);
        }
        in.close();
        // Rotina para ordenacao
        int tamanho = lista.size();
        for (int i = 0; i < tamanho-1; i++){
            for (int j = i; j < tamanho; j++){
                if (lista.get(i).area > lista.get(j).area){
                    Figura toMove = lista.get(i);
                    lista.set(i, lista.get(j));
                    lista.set(j, toMove);

                }
            }
        }
        // Rotina para mostrar na tela
        for (Figura item:lista){
            System.out.println(item);
        }

    }
}