public interface Fila{
    public Object inicio();
    public Object fim();
    public void enfileirar(Object o);
    public void desenfileirar();
    public boolean estaVazia();
}