class no {
    Object info;
    no prox;
    no(Object o){
        info = o;
        prox = null;
    }

    public String toString(){
        return info.toString() + "\n";
    }
}

class Lista implements Fila{
    no inicio, fim;

    Lista(){
        inicio = fim = null;
    }

    public Object inicio(){
        if (estaVazia()) return null;
        return inicio.info;
    }

    public Object fim(){
        if (estaVazia()) return null;
        return inicio.info;        
    }

    public boolean estaVazia(){
        if ((inicio.info == null)&&(fim.info)==null){
            return true;
        }
        return false;
    }

    public void enfileirar(Object o){
        no novo = new no(o);
        if (estaVazia()) inicio = fim = novo;
        else {
            fim.prox = novo;
            fim = novo;
        }
    }

    public void desenfileirar(){
        if (!estaVazia()){
            no novo = inicio;
            inicio = inicio.prox;
            if (inicio == null) fim = null;
        }
    }

    public String toString(){
        if (estaVazia()) return null;
        String s = "";
        no p = inicio;
        while (p != null){
            s += p.toString();
            p = p.prox;
        }
        return s;
    }
}