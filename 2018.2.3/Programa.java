// 2018.2.3
import java.util.*;
import java.io.*;

class Numero {
    int numero;
    int frequencia;

    Numero (int numero){
        this.numero = numero;
        this.frequencia = 1;
    }

    public void add(){
        this.frequencia++;
    }
}

public class Programa{
    public static void main(String[] args) throws IOException{
        List<Numero> lista = new ArrayList<Numero>();
        FileReader arquivo = new FileReader(args[0]);
        BufferedReader in = new BufferedReader(arquivo);
        String linha;
        while((linha = in.readLine()) != null){
            boolean achou = false;
            int valor = Integer.parseInt(linha);
            if (lista.isEmpty()){
                lista.add(new Numero(valor));
            } else {
                for (Numero item:lista){
                    if(item.numero==valor){
                        item.add();
                        achou = true;
                    }
                }
                if (!achou){
                    lista.add(new Numero(valor));
                }
            }
        }
        int x = lista.size();
        for (int i=0;i < x-1; i++){
            for(int j=i; j < x; j++){
                if(lista.get(i).frequencia<lista.get(j).frequencia){
                    Numero aux = lista.get(i);
                    lista.set(i, lista.get(j));
                    lista.set(j, aux);
                }
                if((lista.get(i).frequencia==lista.get(j).frequencia) && (lista.get(i).numero>lista.get(j).numero)){
                    Numero aux = lista.get(i);
                    lista.set(i, lista.get(j));
                    lista.set(j, aux);
                }
            }
        }
        for (Numero item:lista){
            System.out.println(item.numero + " " + item.frequencia);
        }
    }
}