import java.io.IOException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.*;

public class Programa {
    public static void main(String[] args) throws IOException{
        String nome = args[0];
        String linha;
        char c;
        BufferedReader in;
        BufferedWriter out;

        in = new BufferedReader(new FileReader(args[0]));
        out = new BufferedWriter(new FileWriter("saida.txt"));

        while( (linha = in.readLine()) != null){
            int tamanho = linha.length();
            int contagem = 0;
            boolean final1 = true;
            for (int i =0; i < tamanho ; i++){
                c = linha.charAt(i);

                if (c=='(') { contagem++; };

                if (c==')') { contagem--; };

                if (contagem < 0){
                    out.write("Incorreta\n");
                    i = tamanho;
                    final1 = false;
                }
            }
            if (contagem == 0){
                out.write("Correta\n");
            }
            if (contagem != 0 && final1 == true){
                out.write("Incorreta\n");
            }
        }
        in.close();
        out.close();
    }
}