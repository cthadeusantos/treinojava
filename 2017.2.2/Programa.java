import java.io.*;
import java.util.*;

public class Programa {
    public static void main(String[] args){
        String nomeArquivo = args[0];
        String indice = args[1];
        String texto = args[2];
        int intIndice = Integer.parseInt(indice);
        if (intIndice <= 0){
            throw new IllegalArgumentException("Índice precisa ser maior que zero");
        }
        try {
            FileReader arquivo = new FileReader(nomeArquivo);
            BufferedReader in = new BufferedReader(arquivo);
            FileWriter arqAuxiliar = new FileWriter("auxiliar.txt");
            BufferedWriter out = new BufferedWriter(arqAuxiliar);
            int contador = 0;
            String linha;
            while((linha = in.readLine()) != null){
                contador++;
                if (contador == intIndice){
                    out.write(texto+"\n");
                } else {
                    out.write(linha+"\n");
                }
            }
            while(contador < intIndice){
                contador++;
                if (contador == intIndice){
                    out.write(texto+"\n");
                } else {
                    out.write("\n");         
                }
            }
            in.close();
            out.close();
            FileReader arquivo1 = new FileReader("auxiliar.txt");
            BufferedReader in1 = new BufferedReader(arquivo);
            FileWriter arqAuxiliar1 = new FileWriter(args[0]);
            BufferedWriter out1 = new BufferedWriter(arqAuxiliar);
            while((linha = in1.readLine())!= null){
                out1.write(linha);
            }
            in1.close();
            out1.close();   
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}