Public class Lista{
    private No primeiro = null;
    private No ultimo = null;
    public void insereOrdenado(int valor){
        No prim = primeiro;
        No novo = new No(valor);
        if (estaVazia()){
            primeiro = ultimo = novo;
            return;
        }
        while (prim != null && prim.valor  <= valor){
            prim =  prim.proximo;
        }
        if (prim==null){
            ultimo.proximo = novo;
            novo.anterior = ultimo;
        }
    };
    public boolean estaVazia(){
        if (this.primeiro == null && this.ultimo == null){
            return true;
        } else {
            return false;
        }
    };
    public void retiraPrimOcorrencia(int valor){};
}