public class MatrizDiagonal extends Matriz {
    private double matriz[][];
    
    public MatrizDiagonal(int tamanho){
        super(tamanho, tamanho);
        matriz = new double[tamanho][tamanho];
        for (int i=0; i < tamanho; i++){
            for (int j =0; j < tamanho; j++){
                if (i!=j){
                    matriz[i][j] =0;
                }
            }
        }
    }

    public void atribuir( int lin, int col, double valor){
        this.validaIndices(lin,col);
        lin--;
        col--;
        if (lin == col){
            matriz[lin][col] = valor;
        } else {
            throw new IllegalArgumentException("Não pode alterar!");
        }
        
    }
    public double obter(int lin, int col){
        if (lin<=0 || lin > this.numeroDeLinhas() || col <= 0 || col > this.numeroDeColunas()){
            throw new IllegalArgumentException("Parametros invalidos!");
        }
        col--;
        return matriz[0][col];
    }
}