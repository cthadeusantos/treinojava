

public abstract class Matriz{
    protected int nLinhas;
    protected int nColunas;

    public Matriz(int nLinhas, int nColunas){
        if( nLinhas <= 0 || nColunas <=0 ){
            throw new IllegalArgumentException("Parâmetros inválidos");
        }
        this.nLinhas = nLinhas;
        this.nColunas = nColunas;
    }

    public int numeroDeLinhas(){
        return this.nLinhas;
    }

    public int numeroDeColunas(){
        return this.nColunas;
    }
    public abstract void atribuir(int lin, int col, double valor);
    public abstract double obter(int lin, int col);
    protected void validaIndices(int lin, int col){
        if (col <= 0 || col > this.numeroDeColunas() || lin<=0 || lin >this.numeroDeLinhas()){
            throw new IllegalArgumentException("Parâmetros inválidos");
        } 
    }
    public String toString(){
        String string = ""; 
        for (int i = 1 ; i <= this.nLinhas; i++){
            for(int j = 1; i <= this.nColunas; j++){
                double z = this.obter(i,j);
                string = string + Double.toString(z) + " ";
            }
            string = string + "\n";
        }
        return string;
    }
}