public class MatrizIdentidade extends Matriz {
    private double matriz[][];
    
    public MatrizIdentidade(int tamanho){
        super(tamanho, tamanho);
        matriz = new double[tamanho][tamanho];
        for (int i=0; i < tamanho; i++){
            for (int j =0; j < tamanho; j++){
                if (i!=j){
                    matriz[i][j] =0;
                } else {
                    matriz[i][j] = 1;
                }
            }
        }
    }

    public void atribuir( int lin, int col, double valor){
        this.validaIndices(lin,col);
        lin--;
        col--;
        if ((lin == col && valor !=1)||(lin!=col && valor!=0)){
            matriz[lin][col] = valor;
        } else {
            throw new IllegalArgumentException("Não pode alterar valores!");
        }
        
    }
    public double obter(int lin, int col){
        if (lin<=0 || lin > this.numeroDeLinhas() || col <= 0 || col > this.numeroDeColunas()){
            throw new IllegalArgumentException("Parametros invalidos!");
        }
        col--;
        lin--;
        return matriz[lin][col];
    }
}