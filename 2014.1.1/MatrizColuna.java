public class MatrizColuna extends Matriz {
    private double matriz[][];
    
    public MatrizColuna(int lin){
        super(lin, 1);
        matriz = new double[1][col];
    }

    public void atribuir( int lin, int col, double valor){
        this.validaIndices(lin,col);
        lin--;
        col--;
        matriz[lin][col] = valor;
    }
    
    public double obter(int lin, int col){
        if (lin<=0 || lin > this.numeroDeLinhas() || col <= 0 || col > this.numeroDeColunas()){
            throw new IllegalArgumentException("Parametros invalidos!");
        }
        lin--;
        return matriz[lin][lin];
    }
}