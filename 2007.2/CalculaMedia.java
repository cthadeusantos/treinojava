// AP2 2007-2

import java.io.*;
import java.util.*;

public class CalculaMedia {
    public static void main(String[] args){
        String NomeArquivo = args[0];
        ArrayList<String> medias = new ArrayList<String>();
        try {
            File myFile = new File(NomeArquivo);
            FileReader fileReader = new FileReader(myFile);

            BufferedReader reader = new BufferedReader(fileReader);

            String linha = null;
            while ((linha = reader.readLine()) != null){
                String[] notas = linha.split(" ");
                Float mediaaluno = (Float.parseFloat(notas[1]) +
                                    Float.parseFloat(notas[2]) +
                                    Float.parseFloat(notas[3])
                                    ) / 3;
                medias.add(Float.toString(mediaaluno));
            }
            reader.close();
            Collections.sort(medias);
            for (String item:medias){
                System.out.println(item);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}