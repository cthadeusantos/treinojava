import static java.lang.Math.*;

abstract class Quadrilatero {
    protected double lado1, lado2, lado3, lado4;
    public Quadrilatero(double l1, double l2, double l3, double l4) {
                lado1 = l1; lado2 = l2; lado3 = l3; lado4 = l4;
    }
    public abstract double perimetro();
}

class Retangulo extends Quadrilatero {
    public Retangulo(double b, double h) {
        super(b, h, b, h);
    }
    public void exibe() {
        System.out.println("Retangulo com lados " + lado1 + " e " + lado2);
    }
    public double perimetro() {
        return (this.lado1 * 2) + (this.lado2 * 2);
    }
}

class Quadrado extends Quadrilatero {
    public Quadrado(double b) {
        super(b, b, b, b);
    }
    public double perimetro(){
        return (this.lado1 * 4);
    }
}

class Circulo {
    private double raio;
    public Circulo(double r1){
        raio = r1;
    }
    public double perimetro(){
        return (2 * this.raio * Math.PI);
    }
}
public class Programa{
    public static void main(String[] args){

    }
}
