abstract class Matriz {
    protected int[][] matriz;
    protected int nLinhas;
    protected int nColunas;

    public Matriz(int lin, int col){
        if (lin <=0 || col <=0){
            throw new IllegalArgumentException("Parâmetros de entrada inválidos para a matriz!");
        }
        this.nLinhas = lin;
        this.nColunas = col;
    }
    public abstract obter(int lin, int col);
    public abstract void atribuir(int lin, int col, double valor);
    public int numeroDeLinhas() {
        return this.nLinhas;
    }
    public int numeroDeColunas() {
        return this.nColunas;
    }
}

class MatrizLinha extends Matriz {
    public MatrizLinha(int col){
        super(1, col);
        Matriz matriz = new Matriz[0][col-1];
    }
    public void atribuir(int lin, int col, double valor){
        col--;
        this.matriz[0][col] = valor;
    }
    public double obter(int lin, int col){
        if (lin<=0 || lin > this.numeroDeLinhas() || col <= 0 || col > this.numeroDeColunas()){
            throw new IllegalArgumentException("Parametros invalidos!");
        }
        col--;
        return this.matriz[0][col];
    }
}

public static void main(String[] args){
    Matriz a = new MatrizLinha(1);
    a.atribuir(1, 1, 10);
    a.obter(1,1);
}