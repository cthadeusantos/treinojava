// AP2 2014.2.1
public class Programa{
    public static void main (String[] args)  throws Exception{
        Eletronico[] vet = new Eletronico[4];
        vet[0] = new Bluray("FabBRP", "PAL-M", 100F, true, 1);
        vet[1] = new Bluray("FabBRP", "NTSC", 200F, false, 2);
        vet[2] = new TV("FabTV", "PAL-M", 300F, 43, true);
        vet[3] = new TV("FabTV", "NTSC", 40F, 29, false);
        for(int i = 0; i < vet.length; i++) System.out.println(vet[i]);
    }
}