
public abstract class Eletronico {
    static int codigo = 0;
    final String fabricante;
    final String sistCor;
    final double preco;
    public Eletronico(String fabricante, String sistCor, double preco){
        this.fabricante = fabricante;
        this.sistCor = sistCor;
        this.preco = preco;
        if (preco < 0){
            throw new DadoInvalidoException("Valor do preço inválido!");
        }
        if (fabricante == null || fabricante.equals("")){
            throw new DadoInvalidoException("Fabricante inválido!");
        }
        if (sistCor == null || sistCor.equals("") || (!sistCor.equals("NTSC") && !sistCor.equals("PAL-M"))){
            System.out.println(sistCor);
            throw new DadoInvalidoException("Sistema de cor inválido!");
        }
        codigo++;
    }
    public abstract String toString();
}