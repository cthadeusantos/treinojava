public class Bluray extends Eletronico{
    private boolean mp3;
    private int regiao;
    private String x;
    public Bluray(String fabricante, String sistCor, double preco, boolean mp3, int regiao){
        super(fabricante, sistCor, preco);
        this.mp3 = mp3;
        this.regiao = regiao;
        if (regiao < 1 || regiao > 4){
            throw new DadoInvalidoException("REgiao invalida!");
        }
    }
    @Override
    public String toString(){
        if (this.mp3){
            x = "Sim";
        } else {
            x = "Não";
        }
        return  "Bluray Fabricante: " + this.fabricante +
                "\nSistema Cor: " + this.sistCor +
                "\nPreço: " + this.preco +
                "\nReproduz MP3: " + x + 
                "\nRegião: " + this.regiao;
    }
}