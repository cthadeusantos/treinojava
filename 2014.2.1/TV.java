public class TV extends Eletronico {
    private boolean TVplana;
    private double tamanho;
    public TV(String fabricante, String sistCor, double preco, double tamanho, boolean TVplana){
        super(fabricante, sistCor, preco);
        this.TVplana = TVplana;
        this.tamanho = tamanho;
        if (tamanho < 0){
            throw new DadoInvalidoException("Tamanho da tela invalida!");
        }
    }
    @Override
    public String toString(){
        String x;
        if (this.TVplana){
            x = "Sim";
        } else {
            x = "Não";
        }
        return  "TV Fabricante: " + this.fabricante +
                "\nSistema Cor: " + this.sistCor +
                "\nPreço: " + this.preco +
                "\nTela Plana: " + x + 
                "\nTamanho: " + this.tamanho;
    }
}