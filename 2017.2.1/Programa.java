import java.util.*;

public class Programa {
    public static void main (String[] args){
        List<Comodo> apto = new ArrayList<Comodo>();
        apto.add(new Quarto(10,2,3));
        apto.add(new Cozinha(10,2,3));
        double total = 0;
        for (Comodo c : apto){
            total += c.area_ceramica();

        }
        System.out.println(total);
    }
}