public class Cozinha extends Comodo {
    public double areaceramica;
    public Cozinha(double l1, double l2, double a){
        super(l1, l2, a);
    }
    @Override
    public double area_ceramica(){
        return super.area() + (super.perimetro() * 2 ) * this.pe_direito;
    }
}