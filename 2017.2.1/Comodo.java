public abstract class Comodo {
    double lado1, lado2, pe_direito;
    public Comodo (double l1, double l2, double a) {
        this.lado1 = l1;
        this.lado2 = l2;
        this.pe_direito = a;
    }
    public double perimetro() {
        return lado1 + lado2;
    }
    public double area() {
        return lado1 * lado2;
    }
    abstract double area_ceramica();
}