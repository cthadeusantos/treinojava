// AP2 2016.2 Questao 2
import java.util.ArrayList;
import java.util.List;

class Pedido {
    int codigoPrato;
    double preco;
    public Pedido(int codigoPrato, double preco){
        this.codigoPrato = codigoPrato;
        this.preco = preco;
    }
    public double getPrecoPrato(){
        return this.preco;
    };
}

class PedidoViagem extends Pedido{
    String id;
    public PedidoViagem(int codigoPrato, double preco, String id){
        super(codigoPrato, preco);
        this.id = id;
    }
    public double getPrecoPrato(){
        return super.getPrecoPrato()*0.1;
    }; 
}
class PedidoLocal extends Pedido{
    int id;    
    public PedidoLocal(int codigoPrato, double preco, int id){
        super(codigoPrato, preco);
        this.id = id;
    }
}

public class Programa{
    public static void main(String[] args){
        List<Pedido> pedidos = new ArrayList<Pedido>();
        pedidos.add(new PedidoLocal(1, 50.0, 1));
        pedidos.add(new PedidoLocal(2, 20.0, 1));
        pedidos.add(new PedidoLocal(3, 100.0, 1));
        pedidos.add(new PedidoViagem(1, 50.0, "9999-9999"));
        pedidos.add(new PedidoViagem(2, 20.0, "8888-8888"));
        double soma = 0;
        for (Pedido p : pedidos){
            soma = soma + p.getPrecoPrato();
        }
        System.out.println("A soma é: " + soma);
    }
}