// AP2 2007.2.3
import java.io.*;

public class ContaPalavra {
    public static void main(String[] args){
        String palavra = args[1];
        String nome_arquivo = args[0];
        int contador = 1;
        BufferedReader arquivo = null;
        String palavra_lida;
        try {
                 arquivo = new BufferedReader(new FileReader(nome_arquivo)); 
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo nao encontrado");
        }
        try {
            while ((palavra_lida = arquivo.readLine()) != null){
                if (palavra_lida.contains(palavra)) {
                    System.out.print(Integer.toString(contador) + " ");
                }
                contador++;
            }
        } catch (IOException e) {
        }
   }
}
