// Versao 1 - usando array
// da prova AP2 2007.1

import java.io.*;
public class learquivo {
    public static void main(String[] args){
        String nomearquivo = args[0];
        BufferedReader arquivo = null;
        float acumulador = 0;
        int contador = 0;
        int indice = 0;
        float[] mediaaluno = new float[120];

        try {
            arquivo = new BufferedReader(new FileReader(nomearquivo));
            String str;

            while ( (str = arquivo.readLine()) != null){
                indice = contador/3;
                mediaaluno[indice] = mediaaluno[indice] + Float.parseFloat(str);
                contador++;
                if (contador%3 == 0){
                    mediaaluno[indice] = mediaaluno[indice] / 3;
                    acumulador = acumulador + mediaaluno[indice];
                    System.out.println("Média do aluno " + Integer.toString(indice) + ": "+Float.toString(mediaaluno[indice]));
                }
            }
            System.out.println("Média da turma: " + Float.toString(acumulador/(contador/3)));
            arquivo.close();
        } catch(IOException e){}
    }
}